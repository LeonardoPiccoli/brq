﻿using ItDevRisk.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ItDevRisk
{
    class Program
    {
        private static double ValueRisk { get; set; } //indicates the transaction amount in dollars
        private static List<Client> client = new List<Client>();

        private static List<ClientRisk> clientRisk = new List<ClientRisk>();
        private static DateTime DataReferencia { get; set; } 
        private static int QtdTrade { get; set; }

        static void Main(string[] args)
        {
            SetValuesDefault();
            
            Console.WriteLine("Question 1");
            Console.WriteLine("Wating Input");

            Input();
        }

        private static void SetValuesDefault()
        {
            ValueRisk = 1000000;
            client.Add(new Client() { Sector = "private", Category = "RIGHRISK", IsPoliticallyExposed=false });
            client.Add(new Client() { Sector = "public", Category = "MEDIUMRISK",IsPoliticallyExposed = false });
            client.Add(new Client() { Sector = "DEFAULT", Category = "DEFAULT",IsPoliticallyExposed = false });
            client.Add(new Client() { Sector = "PEP", Category = "PEP",IsPoliticallyExposed = true });
            
            DataReferencia = DateTime.MinValue;
            QtdTrade = -1;
        }

        private static void Input()
        {
            string input = Console.ReadLine();

            if (DataReferencia == DateTime.MinValue)
                InputDateReference(input);
            else if (QtdTrade == -1)
                InputNumberTrades(input);
            else
            {
                if (input.Contains("show"))
                    ShowProgram();

                else
                    InputAvaliableoRisk(input);
            }
            
                
            
            Input();
            
        }

        private static void ShowProgram()
        {
            Console.WriteLine(string.Format("Date of Reference {0}",DataReferencia.ToString("MM/dd/yyyy")));
            Console.WriteLine(string.Format("Number of Trades {0}",QtdTrade.ToString()));

            foreach (ClientRisk c in clientRisk)
            {
                Console.WriteLine(string.Format("Value {0}, Sector={1}, NextPayment={2}, Category={3}", c.value.ToString(),c.sector, c.nextPayment.ToString("MM/dd/YYYY"), c.risk));
            }

        }

        private static void InputAvaliableoRisk(string input)
        {
            try
            {
                int nTrades = Convert.ToInt32(input.Split(' ')[0]);
                string sector = input.Split(' ')[1];
                DateTime dateOfTrade =  Convert.ToDateTime(DateTime.ParseExact(input.Split(' ')[2], "MM/dd/yyyy", CultureInfo.InvariantCulture));
                string risk = "";
                if (input.Split(' ').Length > 3 && Convert.ToBoolean(Convert.ToInt16(input.Split(' ')[3])))
                    sector = "PEP";
                else if (nTrades < ValueRisk)
                    sector = "DEFAULT";

                
                foreach (Client c in client)
                {
                    if (sector.ToLower() == c.Sector.ToLower())
                    {
                        risk = c.Category.ToUpper();
                        Console.WriteLine(risk);
                    }
                }


                clientRisk.Add(new ClientRisk() { value = nTrades, sector = input.Split(' ')[1], nextPayment = dateOfTrade, risk = risk });

            }
            catch (Exception)
            {
                Console.WriteLine("Input numberOfTrades sector dateOfTrade IsPoliticallyExposed(optional)");

            }
        }
        private static void InputNumberTrades(string input)
        {
            try
            {
                QtdTrade = Convert.ToInt32(input);
                Console.WriteLine(string.Format("Number of Trades = {0}", input));
            }
            catch (Exception)
            {
                Console.WriteLine("Input the number of trades");

            }
        }

        private static void InputDateReference(string input)
        {
            try
            {
                DataReferencia = Convert.ToDateTime(DateTime.ParseExact(input, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                Console.WriteLine(string.Format("Date of Reference = {0}", input));
            }
            catch (Exception)
            {
                Console.WriteLine("Input the date of reference");

            }
        }


    }
}
