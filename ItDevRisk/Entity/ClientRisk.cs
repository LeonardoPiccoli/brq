﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItDevRisk.Entity
{
    public class ClientRisk
    {
        public double value { get; set; }
        public string sector { get; set; }
        public DateTime nextPayment { get; set; }
        public string risk { get; set; }

    }
}
