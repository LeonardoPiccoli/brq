﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItDevRisk.Entity
{
    public class Client
    {
        public string Sector { get; set; }
        public bool IsPoliticallyExposed { get; set; }
        public string Category { get; set; }
    }
}
